FROM r-base

LABEL maintainer="Umesh Mohan <moh@nume.sh>"
  
RUN apt-get update
RUN apt-get install -y libgeos-dev libgdal-dev python3-pip
RUN R -e "install.packages('cleangeo', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('raster', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('rgdal', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('dplyr', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('lubridate', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('tidyr', repos='https://cloud.r-project.org')"
RUN pip3 install tweepy

