library(cleangeo)
library(raster)
library(rgdal)
library(dplyr)
library(lubridate)
library(tidyr)

## paste the various strings to create the url for UTC 2330 hrs = IST 0500 hrs. Add the Julian Day likewise. 
## As the release of the 2330 hrs nowcast is one day later in UTC, subtract one day from todays UTC date
landslide_url <-  paste0("https://pmmpublisher.pps.eosdis.nasa.gov/products/global_landslide_nowcast_3hr/export/Global/",format(now(tzone="UTC")-days(1), "%Y"), "/", format(now(tzone="UTC")-days(1), "%j"), "/", "global_landslide_nowcast_3hr.", format(now(tzone="UTC")-days(1), "%Y%m%d"), ".233000.geojson")


## DOWNLOAD THE FILE
download.file(landslide_url, destfile = "latest_landslide_nowcast_file.geojson")


script <- readLines("script.js")

## REPLACE THE DAY NUMBER THAT IS DISPLAYED IN THE LEGEND
script_update  <- gsub(pattern = "day", replace = paste0(format(now(tzone="UTC"), "%d")) , x = script)

writeLines(script_update, con="script.js")

script <- readLines("script.js")

script_update2  <- gsub(pattern = "month", replace = paste0(month(now(tzone="UTC"), label=TRUE, abbr=FALSE)) , x = script)

## REPLACE THE FILE
writeLines(script_update2, con="script.js")

script <- readLines("script.js")

script_update3  <- gsub(pattern = "year", replace = paste0(year(now(tzone="UTC"))) , x = script)

writeLines(script_update3, con="script.js")


## CREATE THE LANDSLIDE FILE
landslide <- readOGR("latest_landslide_nowcast_file.geojson")

## ADD THE ROWS TO THE DF OTHERWISE THE FOLLOWING ERROR COMES UP
## Error in `rownames<-`(x, value) :attempt to set 'rownames' on an object with no dimensions
landslide@data$id <- 1:nrow(landslide)

## CALL THE CIRCLES FILE
circles <- readOGR("arunachal-pradesh-circles.geojson")

## CALL THE DISTRICTS FILE
districts <- readOGR("arunachal-pradesh-districts.geojson")
districts <- as.data.frame(districts)
 
## CLEAN THE GEOMETRIES OF THE LANDSLIDE FILE
landslide_cleaned <- clgeo_Clean(landslide, errors.only = NULL, strategy = "POLYGONATION", verbose = FALSE)


## INTERSECT THE LANDSLIDE FILE WITH THE CIRCLES
intersectit <- raster::intersect(landslide_cleaned, circles)


## MAKE DATA FRAME OF THE SPATIAL DATAFRAME
landslide_circles <- as.data.frame(intersectit)

	if(nrow(landslide_circles) > 1)
{

	## USE DPLYR TO SELECT JUST CIRCLE AND DISTRICT, THEN ARRANGE ALPHABETICALLY BY DITRICT AND DROP DUPLICATES
	table_landslide_circles <- landslide_circles %>% select(circle, district) %>% arrange(district) %>% distinct(circle, district)


	## AGGREGATE ALL THE CIRCLES OF A DISTRICT INTO THE SAME ROW AS ITS DISTRICT BUT IN A NEW COLUMN
	aggregate_by_district <- aggregate(table_landslide_circles$circle, list(table_landslide_circles$district), function(x) paste0(toString(unique(x))))


	## RENAME THE COLUMN NAMES
	names(aggregate_by_district) <- c("district", "circles")


	## ADD NEW COLUMN WITH DISTRICT
	aggregate_by_district$text <- "district :"

	aggregate_by_district$new_line1 <- "\n"
	aggregate_by_district$new_line2 <- "\n"
	aggregate_by_district$new_line3 <- "\n"
	aggregate_by_district$new_line4 <- "\n"
	aggregate_by_district$new_line5 <- "\n"


	aggregate_by_district$intro <- paste(as.character(format(now(tzone="UTC"), "%d-%m-%Y")), "Landslide Nowcast at 0500 hrs IST is moderate/high for following circles of :")

	aggregate_by_district <- aggregate_by_district %>% left_join(districts, by="district") %>% select(intro, new_line1, new_line2, district, text, circles, new_line3, twitter_handle, new_line4, new_line5)

	## UNITE ALL COLUMNS SUCH THAT EVERY ROW IS ONE TWEET
	tweets <- aggregate_by_district %>% unite("tweet", intro, new_line1, new_line2, district, text, circles, new_line3, twitter_handle, new_line4, new_line5, remove = T, sep = " ")

	tweets <- unlist(tweets)

	## AUTOMATE THE FILENAME OF THE LIST OF CIRCLES AT RISK
	filename_tweet <- paste0("circle_list_landslide_nowcast_3hr.", paste(format(now(tzone="UTC"), "%Y%m%d")), ".0500.txt")


	## WRITE THE LINES AS A TEXT FILE
	writeLines(tweets, con=paste(filename_tweet))
	}	else {

 	filename_tweet <- paste0("circle_list_landslide_nowcast_3hr.", paste(format(now(tzone="UTC"), "%Y%m%d")), ".0500.txt")

 	tweets <- paste0("NO WARNINGS TODAY")

	## WRITE THE LINES AS A TEXT FILE
	writeLines(tweets, con=paste(filename_tweet))
    
}